package org.test;

public class User {
    private String idCode;
    private String firstName;
    private String lastName;
    private String email;


    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return idCode.equals(user.idCode);
    }

    @Override
    public int hashCode() {
        return idCode.hashCode();
    }

    @Override
    public String toString() {
        return  firstName + ' ' +
                lastName + ' ' +
                "ID-code='" + idCode + '\'';
    }
}
