package org.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class RepositoryMemoryImpl<T> implements Repository<T> {
    protected List<T> entities = new ArrayList<>();

    @Override
    public Collection<T> findAll() {
        return entities;
    }
    
    @Override
    public T add(T entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public T update(T updatedEntity) {
        ListIterator<T> iterator = entities.listIterator();
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (next.equals(updatedEntity)) {
                iterator.set(updatedEntity);
            }
        }
        return updatedEntity;
    }

    @Override
    public void delete(T entity) {
        ListIterator<T> iterator = entities.listIterator();
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (next.equals(entity)) {
                iterator.remove();
            }
        }
    }
}
