package org.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class BorrowingRepositoryMemoryImpl extends RepositoryMemoryImpl<Borrowing> implements BorrowingRepository {
    @Override
    public Collection<Borrowing> filter(Object o) {
        if (o == null) {
            return new ArrayList<>();
        } else if (o instanceof Book) {
            return entities.stream().filter(borrowing -> borrowing.getBook() == o).collect(Collectors.toList());
        } else if (o instanceof User) {
            return entities.stream().filter(borrowing -> borrowing.getUser() == o).collect(Collectors.toList());
        }
        throw new RuntimeException("Unknown type: " + o.getClass().getName());
    }
}
