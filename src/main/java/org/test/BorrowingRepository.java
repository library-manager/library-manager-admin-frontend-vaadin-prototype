package org.test;

import java.util.Collection;

public interface BorrowingRepository extends Repository<Borrowing>{
    Collection<Borrowing> filter(Object o);
}
