package org.test;

import java.util.Collection;

public interface Repository<T> {
    Collection<T> findAll();

    T add(T entity);

    T update(T entity);

    void delete(T entity);
}
