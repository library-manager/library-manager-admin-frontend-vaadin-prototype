package org.test;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.form.impl.field.provider.ComboBoxProvider;

import javax.servlet.annotation.WebServlet;
import java.time.LocalDate;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private Repository<Book> bookRepository = new RepositoryMemoryImpl<>();
    private Repository<User> userRepository = new RepositoryMemoryImpl<>();
    private BorrowingRepository borrowingRepository = new BorrowingRepositoryMemoryImpl();

    private GridCrud<Book> booksCrud;
    private GridCrud<User> usersCrud;

    final int[] AMOUNTINLIBRARY = {1, 2, 3, 1, 1};
    final int[] TOTALAMOUNT = {1, 2, 3, 1, 1};
    final String[] ISBN = {"9785546987886", "9785646987800", "9785746987821", "9785846987842", "9785946987863"};
    final String[] AUTHORS = {"Marc Champagne", "Fritz Kaffka", "Peet Pauli", "Laura Shigihara", "Sir Arthur Connan File"};
    final String[] TITLE = {"Livre la Monde", "Buch der Welt", "Maailma Raamat", "世界の本", "Book of the world"};
    final String[] EMAIL = {"fred.spring@gmail.com", "marc.summer@mail.ee", "susan.autumn@yahoo.com", "paul.winter@outlook.com", "minty.whoever@yandex.ru"};
    final String[] FIRSTNAME = {"Fred", "Marc", "Susan", "Paul", "Minty"};
    final String[] LASTNAME = {"Spring", "Summer", "Autumn", "Winter", "Whoever"};
    final String[] IDCODE = {"36604150241", "39507150232", " 28410150225", "10201150211", "07803150288"};

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        final TabSheet tabSheet = new TabSheet();
        booksCrud = createTab(tabSheet, bookRepository, Book.class, buildBookImportComponent());
        usersCrud = createTab(tabSheet, userRepository, User.class);

        final Button smallButton = new Button("Fill Database");
        smallButton.addStyleName("small");
        smallButton.addClickListener(clickEvent -> {
            fillDatabase();
            refreshAllGrids();
        });
        smallButton.setDisableOnClick(true);

        layout.addComponent(smallButton);
        layout.addComponents(tabSheet);
        setContent(layout);
    }

    private Component buildBookImportComponent() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        final TextField isbnField = new TextField();
        isbnField.setValue("Enter book ISBN");
        isbnField.setStyleName("isbnfield");
        final Button importBookButton = new Button("Import book from Google Books");

        horizontalLayout.addComponents(isbnField, importBookButton);

        return horizontalLayout;
    }

    private void refreshAllGrids() {
        booksCrud.refreshGrid();
        usersCrud.refreshGrid();
    }

    private void fillDatabase() {
        for (int i = 0; i < TOTALAMOUNT.length; ++i) {
            Book newBook = new Book();
            newBook.setAmountInLibrary(AMOUNTINLIBRARY[i]);
            newBook.setTotalAmount(TOTALAMOUNT[i]);
            newBook.setIsbn(ISBN[i]);
            newBook.setAuthors(AUTHORS[i]);
            newBook.setTitle(TITLE[i]);
            bookRepository.add(newBook);

            User newUser = new User();
            newUser.setEmail(EMAIL[i]);
            newUser.setFirstName(FIRSTNAME[i]);
            newUser.setLastName(LASTNAME[i]);
            newUser.setIdCode(IDCODE[i]);
            userRepository.add(newUser);

            Borrowing newBorrowing = new Borrowing();
            newBorrowing.setBook(newBook);
            newBorrowing.setUser(newUser);
            newBorrowing.setBorrowingDate(LocalDate.now());
            newBorrowing.setReturnDate(LocalDate.now().plusMonths(6));
            borrowingRepository.add(newBorrowing);
        }
    }

    private <T> GridCrud<T> createTab(TabSheet tabSheet, Repository<T> repository, Class<T> cls) {
        return createTab(tabSheet, repository, cls, null);
    }

    private <T> GridCrud<T> createTab(TabSheet tabSheet, Repository<T> repository, Class<T> cls, Component toolbarComponent) {
        final VerticalLayout layout = new VerticalLayout();
        final GridCrud<T> crud = new GridCrud<>(cls);
        crud.getGrid().setHeightByRows(5);
        if (toolbarComponent != null) {
            crud.getCrudLayout().addToolbarComponent(toolbarComponent);
        }
        
        crud.setFindAllOperation(repository::findAll);
        crud.setAddOperation(repository::add);
        crud.setUpdateOperation(repository::update);
        crud.setDeleteOperation(repository::delete);

        final String className = cls.getSimpleName();
        final Label label = new Label(className + " borrowings");

        final GridCrud<Borrowing> borrowingCrud = new GridCrud<>(Borrowing.class);

        borrowingCrud.setFindAllOperation(() -> {
            final T selectedItem = crud.getGrid()
                    .getSelectedItems().stream()
                    .findAny().orElse(null);
            return borrowingRepository.filter(selectedItem);
        });
        borrowingCrud.setAddOperation(borrowingRepository::add);
        borrowingCrud.setUpdateOperation(borrowingRepository::update);
        borrowingCrud.setDeleteOperation(borrowingRepository::delete);

        final CrudFormFactory<Borrowing> formFactory = borrowingCrud.getCrudFormFactory();
        formFactory.setFieldProvider("user", new ComboBoxProvider<>(userRepository.findAll()));
        formFactory.setFieldProvider("book", new ComboBoxProvider<>(bookRepository.findAll()));

        crud.getGrid().addSelectionListener(selectionEvent -> borrowingCrud.refreshGrid());

        layout.addComponents(crud, label, borrowingCrud);
        tabSheet.addTab(layout, className + 's');
        return crud;
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

